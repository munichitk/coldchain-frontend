import Vue from 'vue'
import firebase from 'firebase'
import App from './App.vue'
import router from './router'
import Vuetify from 'vuetify'

Vue.config.productionTip = false
Vue.use(Vuetify)

let app
var firebaseConfig = {
  apiKey: "AIzaSyA_okN9YwputxvFQZUpMG_zwN6yiF_JTuI",
  authDomain: "coldd-bb783.firebaseapp.com",
  databaseURL: "https://coldd-bb783.firebaseio.com",
  projectId: "coldd-bb783",
  storageBucket: "coldd-bb783.appspot.com",
  messagingSenderId: "244856535331",
  appId: "1:244856535331:web:d46923e4d1e93b98d53496"
};
firebase.initializeApp(firebaseConfig);

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
