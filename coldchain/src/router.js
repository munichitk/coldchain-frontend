import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Trackinfo from './views/Trackinfo.vue'
import Profile from './views/Profile.vue'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/tracking',
      name: 'Tracking',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/Tracking.vue')
    },
    {
      path:'/trackinfo',
      name: 'Trackinfo',
      component: Trackinfo
    },
    {
      path:'/profile',
      name: 'Profile',
      component: Profile
    }
  ]
})
